package vn.ankieng.phongungthu.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import vn.ankieng.phongungthu.model.ListDTO;

public class DataBaseHelper extends SQLiteOpenHelper {
	private Context mycontext;
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_LIKE = "like";

	private String[] allColum = { COLUMN_ID, COLUMN_NAME, COLUMN_LIKE};

	private static String DB_NAME = "data_chedoan.sqlite";
	public SQLiteDatabase myDataBase;
	private String DB_PATH;
	private String TABLE_CHEDOAN ="chedoan";

	public DataBaseHelper(Context context) throws IOException {
		super(context, DB_NAME, null, 1);
		this.mycontext = context;
		this.DB_PATH = "/data/data/" + context.getPackageName()
				+ "/databases/";

		boolean dbexist = checkdatabase();
		if (dbexist) {
			// System.out.println("Database exists");
			opendatabase();
		} else {
			System.out.println("Database doesn't exist");
			createdatabase();
			opendatabase();

		}
	}

	public void createdatabase() throws IOException {
		boolean dbexist = checkdatabase();
		if (dbexist) {
			// System.out.println(" Database exists.");
		} else {
			this.getReadableDatabase();
			try {
				copydatabase();
			} catch (IOException e) {
				throw new Error("Error copying database");
			}
		}
	}

	private boolean checkdatabase() {
		// SQLiteDatabase checkdb = null;
		boolean checkdb = false;
		try {
			String myPath = DB_PATH + DB_NAME;
			File dbfile = new File(myPath);
			// checkdb =
			// SQLiteDatabase.openDatabase(myPath,null,SQLiteDatabase.OPEN_READWRITE);
			checkdb = dbfile.exists();
		} catch (SQLiteException e) {
			System.out.println("Database doesn't exist");
		}
		return checkdb;
	}

	private void copydatabase() throws IOException {
		// Open your local db as the input stream
		InputStream myinput = mycontext.getAssets().open(DB_NAME);

		// Path to the just created empty db
//		String outfilename = DB_PATH + DB_NAME;

		// Open the empty db as the output stream
		OutputStream myoutput = new FileOutputStream(
				DB_PATH + DB_NAME);

		// transfer byte to inputfile to outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myinput.read(buffer)) > 0) {
			myoutput.write(buffer, 0, length);
		}

		// Close the streams
		myoutput.flush();
		myoutput.close();
		myinput.close();
	}

	public void opendatabase() throws SQLException {
		// Open the database
		String mypath = DB_PATH + DB_NAME;
		myDataBase = SQLiteDatabase.openDatabase(mypath, null,
				SQLiteDatabase.OPEN_READWRITE);
	}

	public synchronized void close() {
		if (myDataBase != null) {
			myDataBase.close();
		}
		super.close();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	public  ArrayList<ListDTO> getListDTO(){
		ArrayList<ListDTO> items = new ArrayList<>();
		Cursor c = myDataBase.query(TABLE_CHEDOAN, allColum, null, null, null,
				null, null);
		c.moveToFirst();
		while (!c.isAfterLast()) {
			ListDTO listmocs = cursorobject(c);
			items.add(listmocs);
			c.moveToNext();
		}
		return items;
	}

	public  ArrayList<ListDTO> getLikeListDTO(){
		ArrayList<ListDTO> items = new ArrayList<>();
		String where = COLUMN_LIKE + "=\'1\'";
		Cursor c = myDataBase.query(TABLE_CHEDOAN, allColum, where, null, null,
				null, null);
		c.moveToFirst();
		while (!c.isAfterLast()) {
			ListDTO listmocs = cursorobject(c);
			items.add(listmocs);
			c.moveToNext();
		}
		return items;
	}
	private ListDTO cursorobject(Cursor cursor) {
		ListDTO listDTO = new ListDTO();
		listDTO.id = cursor.getInt(0);
		listDTO.name = cursor.getString(1);
		listDTO.like = cursor.getInt(2);
		return listDTO;
	}

	public void updateFavorite(int id, int favorite) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_LIKE,favorite);
		myDataBase.update(TABLE_CHEDOAN, cv, COLUMN_ID + "=\'" + id + "\'", null);
		Log.d("id", id + "");
	}
}
