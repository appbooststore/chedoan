package vn.ankieng.phongungthu;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import vn.ankieng.phongungthu.fragment.LauncherFragment;
import vn.ankieng.phongungthu.fragment.SplashFragment;

public class MainActivity extends AppCompatActivity
        implements SplashFragment.SplashListener {
    private InterstitialAd mInterstitialAd;
    private SplashFragment splashFragment;
    private LauncherFragment launcherFragment;
    private FrameLayout splashLayout;

    private Tracker tracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MobileAds.initialize(this, "ca-app-pub-8023467062725545~4710038316");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            splashFragment = new SplashFragment();
            splashFragment.setListener(this);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.splash, splashFragment).commit();
            overridePendingTransition(0, 0);
        }
        splashLayout = (FrameLayout) findViewById(R.id.splash);

        setTitle(getString(R.string.app_name));

        tracker = ((MyApplication) getApplication())
                .getTracker(MyApplication.TrackerName.APP_TRACKER);
        tracker.enableAdvertisingIdCollection(true);
        tracker.setScreenName("StartApp");
        tracker.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public void onBackPressed() {

            if (!getSharedPreferences("rate", 0).getBoolean("rate", false)) {
                getSharedPreferences("rate", 0).edit().putBoolean("rate", true).apply();
                showDialogRating();
            } else {
                super.onBackPressed();
            }
    }

    @Override
    public void startHomePage() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                .beginTransaction();
        launcherFragment = new LauncherFragment();
        fragmentTransaction.add(R.id.container, launcherFragment);
        fragmentTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.remove(splashFragment);
        fragmentTransaction.commitAllowingStateLoss();
        splashFragment = null;
        if (splashLayout != null) {
            splashLayout.setVisibility(View.GONE);
            splashLayout.removeAllViews();
        }
    }

    public void showDialogRating() {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.title_dialog))
                .setMessage(getString(R.string.rating_app))
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse("market://details?id=" + getPackageName()));
                        startActivity(i);
                        tracker.send(new HitBuilders.EventBuilder().setCategory("UX")
                                .setAction("onclick rating")
                                .setLabel("onclick rating").build());
                        dialog.dismiss();
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })

                .show();
    }
}
