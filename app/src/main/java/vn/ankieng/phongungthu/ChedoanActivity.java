package vn.ankieng.phongungthu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.regex.Pattern;

import vn.ankieng.phongungthu.adapter.ListAdapter;
import vn.ankieng.phongungthu.database.DataBaseHelper;
import vn.ankieng.phongungthu.model.ListDTO;

public class ChedoanActivity extends BaseActivity implements IBase {
    private EditText mEditText;
    private ListView mListView;
    private ListAdapter adapter;
    private ArrayList<ListDTO> items;
    private ArrayList<ListDTO> itemsAll;

    private DataBaseHelper database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chedoan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getPropertiesFromUI();
        setElementFromLayout();
    }

    @Override
    public void getPropertiesFromUI() {
        mEditText = (EditText) findViewById(R.id.edit_search);
        mListView = (ListView) findViewById(R.id.mList);
    }

    @Override
    public void setElementFromLayout() {

        try {
            database = new DataBaseHelper(ChedoanActivity.this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (database != null) {
            database.opendatabase();
            items = database.getListDTO();
            itemsAll = items;
        }

        adapter = new ListAdapter(items,ChedoanActivity.this, database);
        mListView.setAdapter(adapter);

        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int position, int i1, int i2) {
                if (charSequence == null || charSequence.length() == 0) {
                    items = itemsAll;
                } else {
                    ArrayList<ListDTO> itemsResult = new ArrayList<ListDTO>();
                    for (int i = 0; i < itemsAll.size(); i++) {
                        if (removeAccent(itemsAll.get(i).name.toLowerCase())
                                .contains(removeAccent(charSequence.toString().toLowerCase()))) {
                            itemsResult.add(itemsAll.get(i));
                        }
                    }
                    items = itemsResult;
                }
                adapter.setItems(items);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String location = items.get(position).id + ".html";
                Intent i = new Intent(ChedoanActivity.this, DetailActivity.class);
                i.putExtra("location", "file:///android_asset/data/" + location);
                i.putExtra("title", items.get(position).name);
                startActivity(i);
            }
        });
        initBannerAds();
        initFullScreenAds();
        trackScreen(getString(R.string.title_chedoan));
    }

    public static String removeAccent(String s) {
        s = s.replace("đ","d");
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }
}
