package vn.ankieng.phongungthu;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class DetailActivity extends BaseActivity implements IBase {
    private WebView webview_result;
    private AdView mAdView;
    private AdRequest adRequest1;
    private LinearLayout	layout_reLayout;
    private  ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        setTitle(getIntent().getStringExtra("title"));
        getPropertiesFromUI();
        setElementFromLayout();
    }

    @Override
    public void getPropertiesFromUI() {
        webview_result = (WebView) findViewById(R.id.webview_result);

    }

    @Override
    public void setElementFromLayout() {
         dialog = ProgressDialog.show(DetailActivity.this, "",
                "Loading. Please wait...", true);
        webview_result.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                dialog.dismiss();
            }
        });

        String location = getIntent().getStringExtra("location");
        webview_result.loadUrl(location);
        initBannerAds();
        initFullScreenAds();
        trackScreen("detail");

    }
}
