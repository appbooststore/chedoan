package vn.ankieng.phongungthu.model;

public class NavDrawerModel {
	int icon;
	String name;
	
	int location;
	
	
	public NavDrawerModel(int icon, String name, int location) {
		super();
		this.icon = icon;
		this.name = name;
		this.location = location;
	}
	public int getLocation() {
		return location;
	}
	public void setLocation(int location) {
		this.location = location;
	}

	public int getIcon() {
		return icon;
	}
	public void setIcon(int icon) {
		this.icon = icon;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
