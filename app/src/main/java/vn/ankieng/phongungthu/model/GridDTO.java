package vn.ankieng.phongungthu.model;

/**
 * Created by Phan on 4/14/2016.
 */
public class GridDTO {
    public int icon;
    public String title;

    public GridDTO(int icon, String title) {
        this.icon = icon;
        this.title = title;
    }
}
