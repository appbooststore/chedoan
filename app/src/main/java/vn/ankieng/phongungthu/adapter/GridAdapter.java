package vn.ankieng.phongungthu.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import vn.ankieng.phongungthu.R;
import vn.ankieng.phongungthu.model.GridDTO;

/**
 * Created by Phan on 4/14/2016.
 */
public class GridAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<GridDTO> items;

    public GridAdapter(Context mContext, ArrayList<GridDTO> items) {
        this.mContext = mContext;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = new ViewHolder();
        if (view==null){
            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.item_grid,null);
            viewHolder.txt_name = (TextView) view.findViewById(R.id.txt_title);
            viewHolder.img = (ImageView) view.findViewById(R.id.img);
            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.img.setImageResource(items.get(position).icon);
        viewHolder.txt_name.setText(items.get(position).title);
        return view;
    }

    public class ViewHolder {
        TextView txt_name;
        ImageView img;

    }
}
