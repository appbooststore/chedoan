package vn.ankieng.phongungthu.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;

import vn.ankieng.phongungthu.R;
import vn.ankieng.phongungthu.model.NavDrawerModel;


public class DrawerAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<NavDrawerModel> navDrawerItems;

	public DrawerAdapter(Context context, ArrayList<NavDrawerModel> navDrawerItems){
		this.context = context;
		this.navDrawerItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {		
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}


	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@SuppressLint("InflateParams") @Override
	public View getView(int position, View convertView, ViewGroup parent) {

			if (convertView == null) {
				LayoutInflater mInflater = (LayoutInflater)
						context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
				convertView = mInflater.inflate(R.layout.item_list_drawer, null);
			}

			ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon_list_drawer);
			TextView txtTitle = (TextView) convertView.findViewById(R.id.name_func_list_drawer);

			imgIcon.setImageResource(navDrawerItems.get(position).getIcon());        
			txtTitle.setText(navDrawerItems.get(position).getName());
		return convertView;
	}

}

