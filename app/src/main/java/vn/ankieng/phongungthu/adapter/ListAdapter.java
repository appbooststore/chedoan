package vn.ankieng.phongungthu.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import vn.ankieng.phongungthu.R;
import vn.ankieng.phongungthu.database.DataBaseHelper;
import vn.ankieng.phongungthu.model.ListDTO;


@SuppressLint("InflateParams")
public class ListAdapter extends BaseAdapter {

    ArrayList<ListDTO> items;
    Context context;
    private DataBaseHelper dataBaseHelper;

    public ListAdapter(ArrayList<ListDTO> items, Context context,DataBaseHelper dataBaseHelper) {
        super();
        this.items = items;
        this.context = context;
        this.dataBaseHelper = dataBaseHelper;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolderHorizontal viewHolder;
        viewHolder = new ViewHolderHorizontal();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_list_items, null);
            viewHolder.txt_name = (TextView) convertView.findViewById(R.id.txt_name);
            viewHolder.imgFavorite = (ImageView) convertView.findViewById(R.id.favorite);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolderHorizontal) convertView.getTag();
        }
        viewHolder.txt_name.setText(items.get(position).name);

        viewHolder.imgFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (items.get(position).like == 1) {
                    items.get(position).like = 0;
                    setItems(items);
                    dataBaseHelper.updateFavorite(items.get(position).id, 0);
                    notifyDataSetChanged();
                } else {
                    items.get(position).like = 1;
                    setItems(items);
                    dataBaseHelper.updateFavorite(items.get(position).id, 1);
                    notifyDataSetChanged();
                }
            }
        });
        if (items.get(position).like == 1){
            viewHolder.imgFavorite.setImageResource(R.drawable.ic_favorite_on);
        }else{
            viewHolder.imgFavorite.setImageResource(R.drawable.ic_favorite);
        }
        return convertView;
    }

    static class ViewHolderHorizontal {
        TextView txt_name;
        ImageView imgFavorite;

    }

    public  void setItems (ArrayList<ListDTO> items){
        this.items = items;
        notifyDataSetChanged();
    }
}
