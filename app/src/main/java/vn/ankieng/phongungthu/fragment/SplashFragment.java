package vn.ankieng.phongungthu.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import vn.ankieng.phongungthu.R;

public class SplashFragment extends Fragment {

    private SplashListener listener;
    private ImageView imgLoading;
    private Handler handler;

    public void setListener(SplashListener listener) {

        this.listener = listener;
        System.out.println("lis " + listener);
    }

    public interface SplashListener {
        public void startHomePage();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.splash, null);
        handler = new Handler();
        imgLoading = (ImageView) view.findViewById(R.id.loading);

        try {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    listener.startHomePage();
                }
            }, 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }



//    private void insertModule(Map<String, ArrayList<ModuleDTO>> moduleMap) {
//        MenuDAO dao = new MenuDAO(getActivity());
//        try {
//            Iterator iterator = moduleMap.entrySet().iterator();
//            dao.open();
//            while (iterator.hasNext()) {
//                Map.Entry mapEntry = (Map.Entry) iterator.next();
//                String key = (String) mapEntry.getKey();
//                ArrayList<ModuleDTO> list = (ArrayList<ModuleDTO>) mapEntry.getValue();
//                for (int i = 0; i < list.size(); i++) {
//                    dao.insertMenu(key, list.get(i));
//                }
//            }
////			dao.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


}
