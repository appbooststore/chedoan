package vn.ankieng.phongungthu.fragment;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;

import vn.ankieng.phongungthu.ChedoanActivity;
import vn.ankieng.phongungthu.IBase;
import vn.ankieng.phongungthu.LikeActivity;
import vn.ankieng.phongungthu.R;
import vn.ankieng.phongungthu.Utility;
import vn.ankieng.phongungthu.adapter.GridAdapter;
import vn.ankieng.phongungthu.model.GridDTO;

public class LauncherFragment extends Fragment implements IBase {



    private  View view;

    private AdView mAdView;
    private AdRequest adRequest;
    private GridView gridView;
    private GridAdapter adapter;
    private  ArrayList<GridDTO> items;
    private String[] mTitles;
    private TypedArray mIcons;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_launcher, null);

        getPropertiesFromUI();
        setElementFromLayout();



        return view;
    }


    @Override
    public void getPropertiesFromUI() {
        gridView = (GridView) view.findViewById(R.id.grid);

    }

    @Override
    public void setElementFromLayout() {
        mTitles = getResources()
                .getStringArray(R.array.arr_launcher);
        mIcons = getResources().obtainTypedArray(
                R.array.arr_launcher_icon);
        items = new ArrayList<GridDTO>();
        for (int i = 0; i < mTitles.length; i++) {
            items.add(new GridDTO(mIcons
                    .getResourceId(i, -1), mTitles[i]));
        }
        mIcons.recycle();
        adapter = new GridAdapter(getActivity(),items);
        gridView.setAdapter(adapter);


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (position==0){
                    Intent i = new Intent(getActivity(), ChedoanActivity.class);
                    startActivity(i);
                }else if (position==1){
                    Intent i = new Intent(getActivity(), LikeActivity.class);
                    startActivity(i);
                }else if (position==2){
                    Utility.gotoOtherApp(getContext(), "Sổ tay phòng bệnh", getString(R.string.package_sotay));
                }else if (position==3){
                    Utility.gotoOtherApp(getContext(), "Xem Tử Vi", getString(R.string.package_tuvi));
                }else if (position==4){
                    Utility.gotoOtherApp(getContext(), "Gieo quẻ bói", getString(R.string.package_gieoque));
                }else if (position==5){
                    Utility.gotoOtherApp(getContext(), "Lịch Vạn Niên", getString(R.string.package_calendar));
                }
            }
        });

        adRequest = new AdRequest.Builder().build();
        mAdView = (AdView) view.findViewById(R.id.adView);
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
            }
        });
    }
}
