package vn.ankieng.phongungthu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


public class BaseActivity extends AppCompatActivity {
	protected Tracker tracker;
	protected InterstitialAd mInterstitialAd;
	protected AdView mAdView;
	protected AdRequest adRequest;
	protected volatile boolean isRunning = true;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		overridePendingTransition(R.anim.open_next, R.anim.close_main);

	}

	@Override
	public void onBackPressed() {
		if (mInterstitialAd!=null) {
			if (mInterstitialAd.isLoaded()) {
				mInterstitialAd.show();
				BaseActivity.this.getSharedPreferences("showAds", 0).edit().putInt("showAds", 4).commit();
				return;
			}
		}
		super.onBackPressed();
		overridePendingTransition(R.anim.open_main, R.anim.close_next);
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	protected void trackScreen(String Screen){
		tracker = ((MyApplication) getApplication())
				.getTracker(MyApplication.TrackerName.APP_TRACKER);
		tracker.enableAdvertisingIdCollection(true);
		tracker.setScreenName(Screen);
		tracker.send(new HitBuilders.AppViewBuilder().build());
	}

	protected void trackEvent(String event){
		tracker.send(new HitBuilders.EventBuilder().setCategory("UX")
				.setAction(event)
				.setLabel(event).build());
	}
	protected void initFullScreenAds(){
		int dem = BaseActivity.this.getSharedPreferences("showAds",0).getInt("showAds",2);
		if (dem!=0){
			BaseActivity.this.getSharedPreferences("showAds",0).edit().putInt("showAds",--dem).commit();
			return;
		}
		mInterstitialAd = new InterstitialAd(this);
		mInterstitialAd.setAdUnitId(getString(R.string.fullscreen_ad_unit_id));

		mInterstitialAd.setAdListener(new AdListener() {
			@Override
			public void onAdClosed() {
				super.onAdClosed();
				finish();
			}

			@Override
			public void onAdLoaded() {
				super.onAdLoaded();
			}

			@Override
			public void onAdFailedToLoad(int errorCode) {
				super.onAdFailedToLoad(errorCode);
			}
		});
		requestNewInterstitial();
	}
	protected void requestNewInterstitial() {
		AdRequest adRequest = new AdRequest.Builder()
				.build();
		mInterstitialAd.loadAd(adRequest);
	}


	protected void initBannerAds() {
		mAdView = (AdView) findViewById(R.id.adView);
		adRequest = new AdRequest.Builder().build();
		mAdView.loadAd(adRequest);
		mAdView.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				super.onAdLoaded();
				mAdView.setVisibility(View.VISIBLE);
			}
		});
	}




	@Override
	protected void onDestroy() {
		super.onDestroy();
		isRunning = false;

	}
}
