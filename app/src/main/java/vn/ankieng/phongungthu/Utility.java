package vn.ankieng.phongungthu;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

public class Utility {

    public static void gotoOtherApp(final Context context, String title, final String packageApp) {
        PackageManager pm = context.getPackageManager();
        Intent appStartIntent = pm.getLaunchIntentForPackage(packageApp);

        if (null != appStartIntent) {
            appStartIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            context.startActivity(appStartIntent);
        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.setTitle(title);
            alert.setMessage(R.string.extend_request);

            alert.setPositiveButton(R.string.btn_continue, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Bring user to the market or let them choose an app?
                    Intent appStartIntent = new Intent(Intent.ACTION_VIEW);
                    appStartIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    appStartIntent.setData(Uri.parse("market://details?id="+packageApp ));

                    context.startActivity(appStartIntent);

                    dialog.dismiss();
                }
            });

            alert.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            alert.show();
        }
    }
}
