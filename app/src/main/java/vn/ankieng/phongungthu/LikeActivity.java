package vn.ankieng.phongungthu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.IOException;
import java.util.ArrayList;

import vn.ankieng.phongungthu.adapter.ListAdapter;
import vn.ankieng.phongungthu.database.DataBaseHelper;
import vn.ankieng.phongungthu.model.ListDTO;

public class LikeActivity extends BaseActivity implements IBase {
    private ListView mListView;
    private ListAdapter adapter;
    private ArrayList<ListDTO> items;
    private DataBaseHelper database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_like);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getPropertiesFromUI();
        setElementFromLayout();
    }

    @Override
    public void getPropertiesFromUI() {
        mListView = (ListView) findViewById(R.id.mList);

    }

    @Override
    public void setElementFromLayout() {
        try {
            database = new DataBaseHelper(LikeActivity.this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (database != null) {
            database.opendatabase();
            items = database.getLikeListDTO();
        }

        adapter = new ListAdapter(items,LikeActivity.this, database);
        mListView.setAdapter(adapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String location = items.get(position).id + ".html";
                Intent i = new Intent(LikeActivity.this, DetailActivity.class);
                i.putExtra("location", "file:///android_asset/data/" + location);
                i.putExtra("title", items.get(position).name);
                startActivity(i);
            }
        });
        initBannerAds();
        initFullScreenAds();
        trackScreen(getString(R.string.title_like));

    }
}
